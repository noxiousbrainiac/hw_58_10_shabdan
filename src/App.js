import React from 'react';
import MyApp from "./components/MyApp/MyApp";

const App = () => {
    return (
        <div>
          <MyApp/>
        </div>
    );
};

export default App;