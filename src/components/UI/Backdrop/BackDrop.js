import React from 'react';
import './Backdrop.css';

const BackDrop = ({show, onClick}) => {
    return  show ?
        <div
            className="Backdrop"
            onClick={onClick}
        /> : null
};

export default BackDrop;