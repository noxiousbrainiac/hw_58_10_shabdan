import React from 'react';
import './Modal.css';
import BackDrop from "../Backdrop/BackDrop";
import Button from "../Button/Button";

const Modal = props => {
    const btnArray = [
        {type: 'Success', label: 'Continue', clicked: ()=> alert('You called Alert')},
        {type: 'Danger', label: 'Close', clicked: props.closeBtn}
    ];

    return (
        <>
            <BackDrop onClick={props.close} show={props.show}/>
            <div
                className={["Modal"].join(' ')}
                style={{
                    transform: props.show ? "translateY(0)" : "translateY(-100vh)",
                    opacity: props.show ? '1' : '0',
                }}
            >
                <div className="ModalHeader">
                    <h3>{props.title}</h3>
                    <Button showBtn={props.closeBtn} btnType="CloseBtn">x</Button>
                </div>
                {props.children}
                <div>
                    {btnArray.map(btn => {
                        return (
                            <Button
                                key={btn.type}
                                btnType={btn.type}
                                showBtn={btn.clicked}
                            >
                                {btn.label}
                            </Button>
                        )
                    })}
                </div>
            </div>
        </>
    );
};

export default Modal;