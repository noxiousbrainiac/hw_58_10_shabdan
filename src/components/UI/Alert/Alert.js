import React from 'react';
import BackDrop from "../Backdrop/BackDrop";
import Button from "../Button/Button";
import './Alert.css';

const Alert = props => {
    return (
        <>
            <BackDrop onClick={props.close} show={props.show}/>
            <div
                onClick={props.changeDismiss}
                className={["Alert", props.type].join(' ')}
                style={{
                    transform: props.show ? "translateY(0)" : "translateY(-100vh)",
                    opacity: props.show ? '1' : '0',
                }}
            >
                <div className="AlertHeader">
                    <h3>{props.title}</h3>
                    {props.dismiss === undefined ? null : <Button showBtn={props.dismiss} btnType="CloseBtn">x</Button>}
                </div>
                {props.children}
            </div>
        </>
    );
};

export default Alert;