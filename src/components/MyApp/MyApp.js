import {useState} from 'react';
import Button from "../UI/Button/Button";
import './MyApp.css';
import Modal from "../UI/Modal/Modal";
import Alert from "../UI/Alert/Alert";

const MyApp = () => {
    const [showModal, setShowModal] = useState(false);
    const [showAlert, setShowAlert] = useState(false);

    const purchaseHandler = () => {
        setShowModal(true);
    }

    const purchaseCancelHandler = () => {
        setShowModal(false);
    }

    const purchaseAlert = () => {
        setShowAlert(true);
    }

    const purchaseCancelAlert = () => {
        setShowAlert(false);
    }

    return (
        <div className="container">
            <Modal
                show={showModal}
                close={purchaseCancelHandler}
                title={"Some kinda modal title"}
                closeBtn={purchaseCancelHandler}
                background={'Orange'}
            >
                <p>This is modal</p>
            </Modal>
            <Alert
                type={"Danger"}
                show={showAlert}
                close={purchaseCancelAlert}
                title={"Attention!"}
                dismiss={purchaseCancelAlert}
            >
                <p>Some text</p>
            </Alert>
            <Button btnType={'Success'} showBtn={purchaseHandler}>Modal</Button>
            <Button btnType={'Danger'} showBtn={purchaseAlert}>Alert</Button>
        </div>
    );
};

export default MyApp;